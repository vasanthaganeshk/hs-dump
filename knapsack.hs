import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

main = BS.interact $ intToByteString . f . map (map readInt . BS.words) . BS.lines

f (x:xs) = memo ! (itemsCount, totWeight)
  where totWeight = head x
        itemsCount= last x
        memorange = ((0, 0), (itemsCount, totWeight))
        memo = listArray memorange
                         (map (uncurry dp) (range memorange))
        dp 0 w = 0
        dp k w
          | w - wk < 0 = memo ! ((k-1), w)
          | otherwise = max (memo ! ((k-1), w))
                            ((memo ! ((k-1), (w-wk))) + vk)
          where wk = weights ! k
                vk = vals ! k
        weights = listArray (1, itemsCount) (map head xs)
        vals = listArray (1, itemsCount) (map last xs)
