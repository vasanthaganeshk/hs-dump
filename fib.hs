import Data.Array

main = interact $ show.fib.read

fib n = memo ! n
  where memo = listArray (1, n) (map myfib [1..n])
	myfib n 
          | n <= 2 = 1
	  | otherwise = memo ! (n-1) + memo ! (n-2)
