import Data.Set as Set
import Data.Char

main = interact $ show.f
f x = size (Set.fromList (Prelude.filter isLetter x))
