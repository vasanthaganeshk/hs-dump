import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

main = interact $ f . g [] . last . words

g ans [] = ans
g ans a@(x:xs)
  | x == 'B' = g ((length (takeWhile ('B'==) a)):ans) (dropWhile ('B'==) a)
  | otherwise = g ans (dropWhile ('W'==) a)

f [] =  "0"
f ans= unlines [(show (length ans)), unwords (reverse (map show ans))]
