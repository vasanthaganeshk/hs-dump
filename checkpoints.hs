import Data.Char

main = interact $ h.head.lines
f = map g
g x
  | x == 'a' = 'z'
  | otherwise = chr ((ord x) - 1)

h x
  | x == (take (length x) ['a', 'a'..]) = (take ((length x)-1) ['a', 'a' ..] ++ "z")
  |otherwise = (takeWhile (=='a') x) ++ (f (takeWhile (/='a') (dropWhile (=='a') x))) ++ (dropWhile (/='a') (dropWhile (=='a') x))
