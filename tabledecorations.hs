import Data.List
import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f.sort.map fastRead .words.head.lines

f a@(x:y:z:[])
  | 2*(x+y) >= z = (sum a) `div` 3
  | otherwise = x+y
