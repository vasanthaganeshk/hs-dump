import qualified Data.Set as Set

sqt x = sqrt (fromIntegral x)
main = interact $ fans.parse.tail.words
fans inp = ans inp (sieve (2:[3,5..(floor (sqt (maximum inp)))]) Set.empty) []

parse x = Prelude.map read x :: [Integer]

sieve (x:xs) tr = sieve (Prelude.filter (\y -> mod y x /= 0) xs) (Set.insert x tr)
sieve [] tr = tr

ans (x : xs) tpr lst
  | (sqt x) - (fromIntegral (floor (sqt x))) > 0.0 = ans xs tpr ("NO" : lst)
  | Set.member (floor (sqt x)) tpr = ans xs tpr ("YES" : lst)
  | otherwise = ans xs tpr ("NO" : lst)

ans [] tpr lst = unlines (reverse lst)
