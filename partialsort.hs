import Data.List

main = interact $ ans.parse.tail.words
parse x = (map read x :: [Integer])
ans lst = f (reverse (g (zip lst [1..(length lst)]) [] 1)) lst

g (x:y:xs) lst orientation
  | orientation == 1 = if (fst x) < (fst y)
                       then g (y:xs) lst 1
                       else g (y:xs) ((snd x):lst) (-1)
  | orientation == (-1) = if (fst x) < (fst y)
                          then g (y:xs) ((snd x):lst) 1
                          else g (y:xs) lst (-1)

g (x:[]) lst orientation = lst

f [] orig = "yes\n1 1"

f (x:[]) orig
  | (take (x-1) orig) ++ (reverse (drop (x-1) orig)) == (sort orig) = "yes\n" ++ (show x) ++ " " ++ (show (length orig))
  | otherwise = "no"

f (x:y:[]) orig
  | (take (x-1) orig)
    ++ (reverse (drop (x-1) (take y orig))) ++
    (drop y orig) == (sort orig) = "yes\n" ++ (show x) ++ " " ++ (show y)
  | otherwise = "no"

f (x:y:xs) orig = "no"
