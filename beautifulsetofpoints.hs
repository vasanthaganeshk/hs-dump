import Numeric

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ g.minimum.map fastRead .words.head.lines

g x = (show (if x == 0 then 0 else (x+1))) ++ "\n" ++ unlines (f [] x 0)

f ans (-1) y = ans
f ans x y
  | x == 0 && (null ans) = ans
  | otherwise = f ((i ++ " " ++ j):ans) (x-1) (y+1)
  where i = show x
        j = show y
