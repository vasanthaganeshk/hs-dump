import Data.Array.Unboxed
import Numeric

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ unlines.g.lines

f (x:[]) ans = reverse ans
f (x:y:xs) ans
  | x == y = f (y:xs) (1:ans)
  | otherwise = f (y:xs) (0:ans)

prefixsum [] sumsofar ans = reverse (sumsofar:ans)
prefixsum (x:xs) sumsofar ans = prefixsum xs (sumsofar+x) (sumsofar:ans)

query (a:b:[]) xs = (xs ! b) - (xs ! a)

g (x:y:xs) = map show (reverse (h k (trtd xs) []))
  where j = (prefixsum (f x []) 0 [])
        k = listArray (1, (length j)) j

h :: Array Int Int -> [[Int]] -> [Int] -> [Int]
h a [] ans = ans
h a (x:xs) ans = h a xs ((query x a):ans)

trtd = map (\x -> (map fastRead (words x)) :: [Int])
