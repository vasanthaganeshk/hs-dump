main = interact $ f.map read .words

f (1:x:xs)
  | x == 0 = "UP"
  | x == 15 = "DOWN"
  | otherwise = "-1"
f (x:xs) = g xs
g xs
  | i == 15 = "DOWN"
  | i == 0  = "UP"
  | i-j < 0 = "DOWN"
  | otherwise = "UP"
  where i = last xs
        j = last (init xs)
