main = interact $ show . g . lines

count n = length . filter (==n)
factorial n = product [1..n]
c n r = (factorial n) `div` ((factorial r)*(factorial (n-r)))

probablity :: Int -> Int -> Double
probablity x a = (fromIntegral (a `c` x)) / (fromIntegral (2^a))

g :: [String] -> Double
g (x:y:[])
  | c > a || d > b = 0.0
  | otherwise = probablity (a-c) q
  where l = length x
        a = count '+' x
        b = l - a
        c = count '+' y
        d = count '-' y
        q = count '?' y
        
