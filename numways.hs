import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.g.parse.tail.words
parse = map fastRead
g xs
  | i `mod` 3 /= 0 = 0
  | otherwise = f (i `div` 3) 0 xs 0 0
  where i = sum xs

f i tsum (x:[]) a1 a2 = a2
f i tsum (x:xs) a1 a2
  | h == i && h == j = f i h xs (a1+1) (a2+a1)
  | h == i = f i h xs (a1+1) a2
  | h == j = f i h xs a1 (a2+a1)
  | otherwise = f i h xs a1 a2
  where h = tsum+x
        j = 2*i
