main = interact $ show . f . map readInt . words

readInt :: String -> Integer
readInt = read

f (k:n:w:[])
  | n >= cost = 0
  | otherwise = cost - n
  where cost = k * (sum [1..w])
