main = interact $ show.f.words

f :: [String] -> Float
f (_:x:y:ys) = (read (takeWhile (/='.') y) :: Float) +
               h (0+(read x)) b b
  where b = (tail (dropWhile (/='.') y))

h :: Int -> String -> String -> Float
h x [] y = read ('0':'.':y) :: Float
h x (y:ys) z
  | i >= 5 = g x z []
  | otherwise = h x ys z
  where i = read (y:[]) :: Int

g :: Int -> String -> String -> Float
g 0 _ ans = read ('0':'.':ans) :: Float
g x [] [] = 0.0
g x [] ans = g x ans []
g x (y:ys) ans
  | ans == [] && i >= 5 = 1.0
  | i >= 5 = g (x-1) j []
  | otherwise = g x ys (ans ++ [y])
  where i = read (y:[]) :: Int
        j = (init ans) ++ (show ((read [(last ans)] :: Int) + 1))
