import Data.Array

main = interact $ show . lis . map read . words

lis xs = dp 0 x
  where x = length xs
        ys = listArray (1, x) xs
        dp _ 0 = 0
        dp prev k
          | cur == 1 && prev == 1 = dp cur (k-1)
          | otherwise = 1 + dp cur (k-1)
          where cur = ys ! k
