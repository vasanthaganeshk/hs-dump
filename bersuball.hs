import Data.List

main = interact $ show.g.pick.f.lines
f xs = map (\x -> (map read (words x)) :: [Integer]) xs
pick (a:b:c:d:[]) = [(sort b), (sort d)]
g (x:y:[]) = merge x y 0
merge [] ys ans = ans
merge xs [] ans = ans
merge (x:xs) (y:ys) ans
  | abs (x - y) <= 1 = merge xs ys (ans + 1)
  | x < y = merge xs (y:ys) ans
  | otherwise = merge (x:xs) ys ans
