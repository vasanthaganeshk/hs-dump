import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f.parse.words
parse = map fastRead

g [] y temp ans = ans
g (x:xs) y temp ans
  | (temp+x) > y = g xs y 0 (ans+1)
  | otherwise = g xs y (temp+x) ans

f (x:y:z:xs) = g (filter (<=y)  xs) z 0 0
