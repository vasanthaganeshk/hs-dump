import qualified Data.Set as Set

main = interact (f.Set.fromList.tail.tail.words)
f x
  | Set.member "C" x = "#Color"
  | Set.member "M" x = "#Color"
  | Set.member "Y" x = "#Color"
  | otherwise    = "#Black&White"
