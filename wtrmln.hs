f :: Integer -> String
f inp =
  if inp < 2 || odd inp
  then "NO"
  else "Yes"

main = interact (f . read)

