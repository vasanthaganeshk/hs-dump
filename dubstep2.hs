main = interact $ clean.split.spliti

spliti ('W':'U':'B':xs) = split xs
spliti x = x
split ('W':'U':'B':xs) = ' ':split (spliti xs)
split (x:xs) = x:(split xs)
split x = x

clean str
  | last str == ' ' = init str
  | otherwise = str
