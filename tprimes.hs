import           Data.List
import qualified Data.Set as Set

main = interact $ unlines . f . tail . map readInt . words

readInt :: String -> Integer
readInt = read

delsundsieve n = filter (<=n) [i+j+2*i*j| i <- [1..e1], let i' = fromIntegral i, j<- [1..(floor ((n' - i')/(2*i' + 1)))]]
  where n' = fromIntegral n
        e1 = floor (sqrt (n' / 2))

inv [] ys = []
inv xs [] = xs
inv a@(x:xs) b@(y:ys)
  | x < y = x:(inv xs b)
  | x > y = inv a ys
  | x == y= inv xs ys

prime n = filter (<= (10^6)) (2:(map (\j -> 2*j + 1) (inv [1..n] (sort (delsundsieve n)))))

f xs = map (\j -> if (Set.member j sv) then "YES" else "NO") xs
  where sv = Set.fromDistinctAscList (map (\j -> j^2) (prime (5*10^5)))
  
