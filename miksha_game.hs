main = interact $ f.tail.lines
f inp = g (map (\x -> map read (words x) :: [Integer]) inp) 0 0

g [] m c
  | m > c = "Mishka"
  | m < c = "Chris"
  | otherwise = "Friendship is magic!^^"

g (x:xs) m c
  | (head x) > (head (tail x)) = g xs (m + 1) c
  | (head x) == (head (tail x)) = g xs m c
  | otherwise = g xs m (c+1)
