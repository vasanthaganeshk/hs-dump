main = interact $ g.f
f x = read x :: Integer
g x
  | y < 5 = "-1"
  | y `rem` 4 /= 0 = (show a) ++ " " ++ (show (a+1))
  | otherwise = (show (b-1)) ++ " " ++ (show (b+1))
  where a = y `div` 2
        b = y `div` 4
        y = x*x

