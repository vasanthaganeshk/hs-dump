import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

main = BS.interact $ intToByteString . gp . map readInt . BS.words

gp (x:k:ft:xs) = dp (x-1) 3 ft
  where xss = listArray (1, x-1) xs
        dp 0 _ _ = 0
        dp n 3 t = la + lb
          where la = dp (n-1) 2 val
                lb = dp (n-1) 3 t
                val= xss ! n
        dp n 2 t
          | t `mod` k == 0 && t `div` k == val = la + lb
          | otherwise = lb
          where la = dp (n-1) 1 val
                lb = dp (n-1) 2 t
                val= xss ! n
        dp n 1 t
          | t `mod` k == 0 && t `div` k == val = 1 + lb
          | otherwise = lb
          where lb = dp (n-1) 1 t
                val= xss ! n
