import Data.Set

main = interact $ g.f
f inp = (Prelude.map (\x -> Prelude.map read (words x) :: [Integer]) (lines inp))

a (x:y:z:[]) = head x
b (x:y:z:[]) = tail y
c (x:y:z:[]) = tail z

g inp
  | fromList [1..(a inp)] == union (fromList (b inp)) (fromList (c inp)) = "I become the guy."
  | otherwise = "Oh, my keyboard!"
