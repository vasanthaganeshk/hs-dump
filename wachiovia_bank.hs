import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

main = BS.interact $ BS.unlines . map sentence . g . map (map readInt) . map BS.words . tail . BS.lines

f :: [[Int]] -> Int
f (x:xs) = memo ! (countItem, totalWeight)
  where memo = listArray ((0, 0), (countItem, totalWeight))
                         (map (\(i, j) -> (dp i j))
                              (range ((0,0),(countItem, totalWeight))))
        dp 0 w = 0
        dp k w
          | w - (weights ! k) < 0 = (memo ! ((k-1), w))
          | otherwise = max (memo ! ((k-1), w))
                            ((memo ! ((k-1), (w- (weights ! k)))) + (val ! k))

        weights = listArray (1, countItem) (map head xs)
        val = listArray (1, countItem) (map last xs)
        totalWeight = head x
        countItem   = last x

g [] = []        
g (x:xs) = (f (x:(take countItem xs))):g(drop countItem xs)
  where countItem = last x
  
sentence :: Int -> BS.ByteString
sentence x = BS.concat [(BS.pack "Hey stupid robber, you can get "),
                        (intToByteString x),
                        (BS.pack ".")]
