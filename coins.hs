main = interact $ show.f. map read .words
f (x:xs) = dp x xs

--dp money a ans | money < 0 || null a = 10000000000
--dp 0 a ans = 0
--dp money a@(coin:coins) ans = min (1 + (dp (money-coin) a)) (dp money coins)

dp money a | money < 0 || null a = [1..1000]
dp 0 a = []
dp money a@(coin:coins)
  | (length i) <= length j = i
  | otherwise = j
  where i = (coin:(dp (money-coin) a))
        j = (dp money coins)
