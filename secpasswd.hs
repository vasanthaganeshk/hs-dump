import Data.List

main = interact $ f . lines

f (x:xs) = g wait
             k
             (takeWhile (<=k) (sort (init (map length xs))))
  where wait = read (last (words x)) :: Int
        k = length (last xs)

g wait passwd lstpasswd = show (i + if i <= wait
                                    then 0
                                    else (5*(if (i `mod` wait) > 0
                                             then (i `div` wait)
                                             else (i `div` wait) - 1)))
                          ++
                          " "
                          ++
                          show (j + if j <= wait
                                    then 0
                                    else (5*(if (j `mod` wait) > 0
                                             then (j `div` wait)
                                             else (j `div` wait) - 1)))
                          
  where i = length (takeWhile (<passwd) lstpasswd) + 1
        j = length lstpasswd
        
