ans i j num
  | null j = num
  | otherwise = ans i (tail j) (num + (length (filter (\x -> (head j) == x) i)))

game a = ans i j 0
  where i = map (\x -> (head (tail x))) a
        j = map (\x -> (head x)) a

main = interact (show . game . f)
  where f inp = (tail (map (\x -> (map (\y -> read y :: Integer)
                                   (words x)))
                        (lines inp)))                
