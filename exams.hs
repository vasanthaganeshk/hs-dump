import Data.List
import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.g (-1) .sort.parse.tail.lines

parse = map (map fastRead.words)

g best [] = best
g best (x:xs)
  | best <= i = g i xs
  | otherwise = g j xs
  where j = maximum x
        i = minimum x
