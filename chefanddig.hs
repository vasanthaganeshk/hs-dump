import Data.Char
main = interact $ unlines.g.tail.words

parse = map (\x -> (ord x) - 48)

f x
  | y == 1 = "Yes"
  | y == (length x) - 1 = "Yes"
  | otherwise = "No"
  where y = sum x

g :: [String] -> [String]
g x = map (\j ->(f (parse j))) x
