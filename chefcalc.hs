import qualified Data.Set as Set
import Numeric

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ unlines.reverse.f [] .parse.tail.lines

parse :: [String] -> [[Int]]
parse = map (map fastRead.words)

f ans [] = ans
f ans (x:xs) = f ((g (take i xs)):ans) (drop i xs)
  where i = head x

g x
  | (maxrepeat j) = "tie"
  | otherwise = if i == 1 then "chef" else show i
  where j = map h x
        i = snd (maximum (zip j [1..]))
h x = (head x) + (q (Set.size (Set.fromList (tail x))) 0)

q x ans
  | x >= 6 = q (x `rem` 6) (ans + (x `div` 6)*4)
  | x >= 5 = q (x `rem` 5) (ans + (x `div` 5)*2)
  | x >= 4 = q (x `rem` 4) (ans + (x `div` 4))
  | otherwise = ans

maxrepeat (x:[]) = False
maxrepeat (x:y:xs) = x == y
