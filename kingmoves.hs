main = interact $ show.g.head.lines

g a@(x:y:xs)
  | a == "a1" ||a == "a8" || a == "h1" || a == "h8" = 3
  | x == 'a' || x == 'h' || y == '1' || y == '8' = 5
  | otherwise = 8
