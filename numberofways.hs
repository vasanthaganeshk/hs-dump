import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f.a.parse.tail.words
parse = map fastRead

g [] x y temp z = z
g (w:ws) x y temp z
  | x == y && w == 0 = g ws x y (temp+1) z
  | x == y && w /= 0 = g ws x w 0 (temp:z)
  | otherwise = g ws x (w+y) temp z

zeroes [] ans = ans
zeroes (x:xs) ans
  | x /= 0 = 0
  | otherwise = zeroes xs (ans+1)

h :: [Integer] -> Integer
h (x:y:[]) = (x+1)*(y+1)
h x = 0

f :: [Integer] -> Integer
f x
  | rem i 3 == 0 = if i == 0 && (k /= 0) then (c k) else (h j)
  | otherwise = 0
  where i = sum x
        j = g x (div i 3) 0 0 []
        k = zeroes x 0

a x = reduce x []
reduce [] ans = ans
reduce (x:[]) ans = reduce [] (x:ans)
reduce (x:y:xs) ans
  | x == 0 = reduce (y:xs) (0:ans)
  | x == (-y) = reduce xs (0:ans)
  | otherwise = reduce (y:xs) (x:ans)

factorial :: Integer -> Integer
factorial n = product [1..n]
c n = (factorial (n-1)) `div` ((factorial (n-3))*2)
