import Numeric

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f.map fastRead .words

f (n:m:a:b:[])
  | m*a <= b = n*a
  | otherwise = i + min b ((n `mod` m)*a)
  where i = (n `div` m)*b
