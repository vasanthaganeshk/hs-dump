main = interact $f . tail . words

f inp = (fadd (unzip3 (parse (map read inp :: [Integer]))))
parse [] = []
parse (x:y:z:xs) = (x, y, z):parse xs

fadd (a, b, c)
  | 0 == sum a && 0 == sum a && 0 == sum a = "YES"
  | otherwise = "NO"
