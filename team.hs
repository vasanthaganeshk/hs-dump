main = interact $ g.map read .words.head.lines

g (x:y:[])
  | a == "-1" && b == "-1" = a
  | a == "-1" = b
  | otherwise = a
  where a = f x y [] 0
        b = f x y [] 1
f 0 0 ans st = ans
f x y ans st
  | x < 0 || y < 0 = "-1"
  | x - y  >= -1 && st == 1 = f x (y-1) ('1':ans) 0
  | st == 1 = f x (y-2) ('1':'1':ans) 0
  | otherwise = f (x - 1) y ('0':ans) 1
