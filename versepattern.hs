main = interact $ parse . tail . lines

parse (x:xs)
  | i == k = "YES"
  | otherwise = "NO"
  where i = (map read (words x)) :: [Int]
        j = map f xs
        k = map (length) j

f = filter (\a -> a == 'a'
                  || a=='e'
                  || a=='i'
                  || a=='o'
                  || a=='u'
                  || a=='y')
