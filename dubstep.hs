split :: [Char] -> [Char] -> [Char]
split str1 str2
  | null str1 = str2
  | ((take 3 str1) == "WUB") && ((last str2) == ' ') = split (drop 3 str1) str2
  | ((take 3 str1) == "WUB") && ((last str2) /= ' ') = split (drop 3 str1) (str2 ++ " ")
  | otherwise = split (tail str1) (str2 ++ [head str1])

clean str
  | last str == ' ' = init str
  | otherwise = str

main = interact $ clean.tail.f
       where f inp = split inp " "
