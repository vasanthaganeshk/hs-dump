import Data.List

main = interact $ f . lines

f (x:xs) = g j
             (last xs)
             (takeWhile (\i -> (length i) <= k) (sort (init xs)))
           where j = read (last (words x)) :: Int
                 k = length (last xs)

{-g wait passwd lstpasswd = show (i + if i <= wait
                                    then 0
                                    else (5*(if (i `mod` wait) > 0
                                             then (i `div` wait)
                                             else (i `div` wait) - 1)))
                          ++
                          " "
                          ++
                          show (j + if j <= wait
                                    then 0
                                    else (5*(if (j `mod` wait) > 0
                                             then (j `div` wait)
                                             else (j `div` wait) - 1)))
                          
  where i = length (takeWhile (\x -> (length x) < k) lstpasswd)
        j = length lstpasswd
        k = length passwd
-}

g wait passwd lstpasswd = (show (i+0)) ++ " " ++ show (j+0)
  where i = length (takeWhile (\x -> (length x) < k) lstpasswd)
        j = length lstpasswd
        k = length passwd
