import Data.Array
main = interact $ show . lis . map read . words
lis xs = dp (maxBound::Int) x
  where x = length xs
        ys = listArray (1, x) xs
        dp _ 0 = 0
        dp s k
          | ys ! k < s = max (1+(dp (ys ! k) (k-1)))
                             (dp s (k-1))
          | otherwise = dp s (k-1)
