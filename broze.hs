main = interact g
g x = f x []
f (x:y:xs) ans = if x == '.'
                 then f (y:xs) ('0':ans)
                 else (if x == '-'
                       then (if y == '.'
                             then f (xs) ('1':ans)
                             else f (xs) ('2':ans))
                        else ans)
f [] ans = ans
