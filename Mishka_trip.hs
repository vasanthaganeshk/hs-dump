main = interact $ show.g.f
f inp = (map (\x -> (map read (words x) :: [Int])) (lines inp))
g inp = (ansp1 (head (tail inp))) + (ans inp 0) - (finsub inp 0)

ansp1 inp = (head inp)*(last inp) + ansp2 inp
ansp2 (x:xs)
  | null xs = 0
  | otherwise = (x)*(head xs) + ansp2 xs

ans (w:x:y:[]) num
  | null y = num
  | otherwise = ans [w, x, (tail y)] (num + sum [(x!!((head y) - 1))*(fst i) | i <- (zip x [1..(head w)]),
                                                 (snd i) /= (head y),
                                                 (snd i) /= (if (head y) - 1 < 1 then (head w) else (head y) - 1),
                                                 (snd i) /= (if (head y) + 1 > (head w) then 1 else (head y) + 1)])
                
finsub (x:y:z:[]) num
  | null (tail z) = num
  | otherwise = finsub [x, y, (tail z)] (sum [(y!!((head z) - 1))*(y!!(j - 1)) | j <- (tail z)])
