import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ j.k.words

f x = (fastRead (g x)) :: Integer
k (x:y:[]) = [(f x), (f y)]

j (x:y:[])
  | x == y = "="
  | x > y  = ">"
  | otherwise = "<"

g ('0':[]) = "0"
g ('0':xs) = g xs
g xs = xs
