import Data.Char
main = interact $ f.g.words
g x = map read x :: [Int]
f (x:y:[])
  | (y==0 && x/=1) || (y>9*x) = "-1 -1"
  | otherwise = (mn x y "") ++ " " ++ (mx x y "")

mn 0 y ans = ans
mn x y ans
  | y > 9 = mn (x-1) (y-9) ('9':ans)
  | y == 1 && x > 1 = mn (x-1) y ('0':ans)
  | y <= 9 && x > 1 = mn (x-1) 1 ((chr (48 + y - 1)):ans)
  | otherwise = mn (x-1) 0 ((chr (48 +y)):ans)

mx 0 y ans = reverse ans
mx x y ans
  | y > 9 = mx (x-1) (y-9) ('9':ans)
  | otherwise = mx (x-1) 0 ((chr (48 + y)):ans)
