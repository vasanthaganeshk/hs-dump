import Numeric (showIntAtBase)
import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

tobin :: Int -> Int
tobin x = read (showIntAtBase 2 (['0'..'1']!!) x "")
bins = map tobin [1, 2..]

main = BS.interact $ g . readInt

g x = BS.concat [intToByteString (fst i),
                 BS.pack "\n",
                 BS.unwords (map intToByteString (snd i))]
  where i = dp x (length j) j
        j = takeWhile (<=x) bins

dp n k vals = memo ! (n, k)
  where memorange = ((0, 0), (n, k))
        memo = listArray memorange (map (uncurry dp) (range memorange))

        dp 0 k = (0, [])
        dp n 0 = (maxBound, [1..])
        dp n k
          | n - nk < 0 = (memo ! (n, (k-1)))
          | otherwise = min (memo ! (n, (k-1)))
                            (1 + (fst i), (nk:(snd i)))
          where i = (memo ! ((n-nk), k))
                nk = values ! k
        values = listArray (1, k) vals
