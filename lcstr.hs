import Data.Array

main = interact $ show . lcs . words

lcs :: [String] -> Int
lcs (s1:s2:[]) = dp x y
  where x = length s1
        y = length s2
        xs = listArray (1, x) s1
        ys = listArray (1, y) s2
        dp :: Int -> Int -> Int
        dp 0 _ = 0
        dp _ 0 = 0
        dp a b
          | xs ! a == ys ! b = maximum [1+(dp (a-1) (b-1)),
                                        dp (a-1) b,
                                        dp a (b-1)]
          | otherwise = max (dp (a-1) b) (dp a (b-1))
