wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
                      "" -> []
                      s' -> w : wordsWhen p s''
                            where (w, s'') = break p s'               

main = interact $ f . last . words

g [] condi temp lstpar lstopar = (lstpar, lstopar)
g ('(':xs) condi temp lstpar lstopar = g xs True [] (temp:lstpar) ('_':lstopar)
g (')':xs) condi temp lstpar lstopar= g xs False [] (temp:lstpar) lstopar
g (x:xs) condi temp lstpar lstopar
  | condi = g xs condi (x:temp) lstpar lstopar
  | otherwise = g xs condi temp lstpar (x:lstopar)

f x = (show (fun1 b)) ++ " " ++ (show (fun2 a 0))
  where i = g x False "" [] []
        a = fst i
        b = snd i

sanmaximum x
  | x == [] = 0
  | otherwise = maximum x

fun1 a = (sanmaximum (map length (wordsWhen (=='_') a)))

fun2 [] ans = ans
fun2 (b:xs) ans = fun2 xs (ans + (length (filter (/="") (wordsWhen (=='_') b))))
