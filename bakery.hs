import qualified Data.Set as Set

main = interact $ show.g.parse.lines

parse xs = map (\x -> (map read (words x)) :: [Integer]) xs
g xs
  | (last (head xs)) == 0 = (-1)
  | otherwise = f (init ys) (Set.fromList (last ys)) infinity
  where ys = tail xs

infinity = (read "Infinity")::Double

f [] ys cost
  | cost == infinity = (-1)
  | otherwise = floor cost
f (x:xs) ys cost
  | (a && (not b)) || ((not a) && b) = f xs ys (min cost c)
  | otherwise = f xs ys cost
  where a = Set.member (head x) ys
        b = Set.member (head (tail x)) ys
        c = fromIntegral (last x)

