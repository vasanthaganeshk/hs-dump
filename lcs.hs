import Data.Array
main = interact $ reverse . snd . dp . words

dp (i:j:[]) = memo ! (li, lj)
  where li = length i
        lj = length j
        vi = listArray (1, li) i
        vj = listArray (1, lj) j
        memorange = ((0, 0), (li, lj))
        memo = listArray memorange (map (uncurry mydp) (range memorange))
        maxlength i j
          | (fst i) >= (fst j) = i
          | otherwise = j
        mydp 0 _ = (0, "")
        mydp _ 0 = (0, "")
        mydp x y
          | (vi ! x) == (vj ! y) = (((fst lst)+1), ((vi ! x):(snd lst)))
          | otherwise = maxlength (memo ! (x, y-1)) (memo ! (x-1, y))
          where lst = memo ! (x-1, y-1)
