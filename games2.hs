main = interact $ show.solve.unzip.parse.tail.f.words
       where f inp = (map read inp :: [Integer])
             parse [] = []
             parse (x:y:z) = (x, y): (parse z)
             solve i = ssolve 0 i
             ssolve ans x
               | null (snd x) = ans
               | otherwise = ssolve (ans + (length (filter (\i -> i==(head (snd x))) (fst x))))
                             ((fst x), (tail (snd x)))
