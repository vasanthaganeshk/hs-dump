splitOn [] an = an
splitOn a an = splitOn (tail (dropWhile (/= (-1)) a))
                       ((takeWhile (/= (-1)) a):an)

readint :: String -> Int
readint = read

main = interact $ g . map (map readint) . map words . tail . lines

g (x:y:[])
  |
  where a = listArray (1, (length x)) x
        b = listArray (1, (length x)) y
