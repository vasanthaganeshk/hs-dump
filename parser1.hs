import Text.ParserCombinators.Parsec hiding (spaces)
import System.Environment
import Data.Either

symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
spaces = skipMany1 space

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool

parseString :: Parser LispVal
parseString = do
  char '"'
  x <- many (noneOf "\"")
  char '"'
  return (String x)

retL (Left a) = a

readExpr :: String -> String
readExpr input
  | isLeft res = "No match: " ++ (show (retL res))
  | otherwise = "Found Value"
  where res :: Either ParseError Char
        res = parse (spaces >> symbol) "Scheme" input

main = do
  (expr:_) <- getArgs
  putStrLn (readExpr expr)
