import qualified Data.Map as Map
main = interact $ show.myfib.readInt

readInt :: String -> Int
readInt = read

myfib :: Int -> Int
myfib n = fib n
  where dict = Map.empty
        fib n = do
          f <- if Map.member dict n
               then dict Map.! n
               else (if n < 2
                     then 1
                     else (fib n-1) + (fib n-1))
          dict <- Map.insert n f dict
          return f
