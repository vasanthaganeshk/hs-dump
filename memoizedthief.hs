import Numeric
import Data.Array

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f. map (map fastRead) .map words .lines

f (x:xs) = memo ! (countItem, totalWeight)
  where memo = listArray ((0, 0), (countItem, totalWeight))
                         (map (\(i, j) -> (dp i j))
                              (range ((0,0),(countItem, totalWeight))))
        dp 0 w = 0
        dp k w
          | w - (weights ! k) < 0 = (memo ! ((k-1), w))
          | otherwise = max (memo ! ((k-1), w))
                            ((memo ! ((k-1), (w- (weights ! k)))) + (val ! k))
        weights = listArray (1, countItem) (map head xs)
        val     = listArray (1, countItem) (map last xs)
        totalWeight = head x
        countItem   = last x
