import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInteger :: BS.ByteString -> Integer
readInteger = fst . fromJust . BS.readInteger
integerToByteString = BUI.toLazyByteString . BUI.integerDec

main = BS.interact $ integerToByteString . g . map readInteger . BS.words

g (n:k:d:[]) = ((f n k) - (f n (d-1))) `mod` ((10^9) + 7)

f a b = memo !  a
  where dp 0 = 1
        dp n | n < 0 = 0
        dp n = sum (map (\x -> sanitizedmemo (n-x)) [1..b])
        memo = listArray (0, a) (map dp [0..a])
        sanitizedmemo n = if n >= 0
                          then memo ! n
                          else 0
