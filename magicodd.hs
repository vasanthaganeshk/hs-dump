main = interact $ unlines.z.y.f.read

y = map (map show)
z = map unwords

ev = [2, 4 ..]
od = [1, 3 ..]
f :: Int -> [[Int]]
f x = g x x ev od (x `div` 2) 1 []

g n 0 tempe tempo st md ans = ans
g n rw tempe tempo st md ans
  | rw > ((n `div` 2) + 1) = g n
                             (rw-1)
                             (drop (2*st) tempe)
                             (drop md tempo)
                             (st-1)
                             (md+2)
                             (((take st tempe) ++ (take md tempo) ++ (take st (drop st tempe))):ans)
  | otherwise = g n
                  (rw-1)
                  (drop (2*st) tempe)
                  (drop md tempo)
                  (st+1)
                  (md-2)
                  (((take st tempe) ++ (take md tempo) ++ (take st (drop st tempe))):ans)
