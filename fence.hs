import Numeric
import Data.Array

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.snd.f. map fastRead .words

f (n:k:ns) = dp (listArray (1, n) ns) (zip [1..] [(k+1)..n]) ((sum (take k ns)), 1)

dp ns [] prev = prev
dp ns (a:as) prev = min prev (dp ns as (w, (i+1)))
  where w = (fst prev) + (ns ! j) - (ns ! i)
        i = fst a
        j = snd a
        
