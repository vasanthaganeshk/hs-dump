import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

main = BS.interact $ BS.unlines .map intToByteString . g . tail . map (map readInt) . map BS.words . BS.lines

g [] = []
g x = (f (take 2 x)):(g (drop 2 x))

f (x:y:[]) = memo ! memorange
  where countBushes = head x
        totWeight = last x
        memorange = (countBushes, totWeight)
        values = listArray (1, countBushes) y
        dp k w
          | k <= 0 = 0
          | (w - (values ! k) < 0) || (k-2 < 0) = memo ! ((k-1), w)
          | otherwise = max (memo ! ((k-1), w))
                            ((values ! k) + (memo ! (k-2, w - (values ! k))))
        memo = listArray ((0, 0), memorange)
                         (map (uncurry dp) (range ((0, 0), memorange)))
