main = interact $ unlines.h.tail.words

f [] [] ans = ans
f (x:xs) (y:ys) ans
  | x == '.' && y == '.' = f xs ys ('a':ans)
  | x == '.' = f xs ys (y:ans)
  | y == '.' = f xs ys (x:ans)
  | x /= y = "-1"
  | otherwise = f xs ys (x:ans)

g x
  | odd (length x) = if a == "-1" then a else (reverse a) ++ q ++ a
  | otherwise = if b == "-1" then b else (reverse b) ++ b
  where i = (length x) `div` 2
        j = (take 1 (drop i x))
        q = if j == "." then "a" else j
        a = f (take i x) (reverse (drop (i+1) x)) []
        b = f (take i x) (reverse (drop i x)) []

h = map g
