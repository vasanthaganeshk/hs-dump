import Data.Array
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.ByteString.Builder as BUI
import Data.Maybe (fromJust)
import qualified Data.Set as Set

readInt :: BS.ByteString -> Int
readInt = fst . fromJust . BS.readInt
intToByteString = BUI.toLazyByteString . BUI.intDec

main = BS.interact $ BS.unlines . map intToByteString . f . parse . tail . BS.lines

parse (x:xs) = ((map readInt (BS.words x)):(map readInt xs):[])

f (x:y:xs) = map (ans ! ) y
  where i = solve Set.empty (reverse x) []
        ans = listArray (1, (length i)) i
  

solve st [] ans = ans
solve st (x:xs) ans = solve newst xs ((Set.size newst):ans)
  where newst = Set.insert x st
