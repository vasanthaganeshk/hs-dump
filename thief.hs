import Numeric
import Data.Array

fastRead :: String -> Int
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f. map (map fastRead) .map words .lines

f (x:xs) = dp (last x) (head x)
  where dp 0 w = 0
        dp k w
          | w - (weights ! k) < 0 = dp (k-1) w
          | otherwise = max (dp (k-1) w)
                            ((dp (k-1) (w- (weights ! k))) + (val ! k))
        weights = listArray (1, (last x)) (map head xs)
        val     = listArray (1, (last x)) (map last xs)
