main = interact $ show.f. map read .words

f (x:xs) = dp x xs

dp sum [] = 0
dp sum a@(x:xs)
  | sum-x >= 0 = 1 + min (dp (sum-x) a) (dp sum xs)
  | otherwise  = 0
