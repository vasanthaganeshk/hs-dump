import Data.List

import Numeric

fastRead :: String -> Integer
fastRead ('-':s) = case readDec s of [(n, "")] -> (-n)
fastRead s = case readDec s of [(n, "")] -> n

main = interact $ show.f.parse.words
parse = map fastRead

g [] cur ans = ans
g (x:xs) cur ans = g xs x (ans + (abs (cur-x)))

f (cnt:x:xs) = minimum [(g (aa++ba) x 0),
                        (g (ca++da) x 0),
                        (g (ab++bb) x 0),
                        (g (cb++db) x 0)]
  where y = (sort xs)
        i = init y
        j = tail y

        aa = (reverse (filter (<=x) i))
        ba = (filter (>x) i)
        ca = (reverse (filter (<=x) j))
        da = (filter (>x) j)

        ab = (filter (>=x) i)
        bb = (reverse (filter (<x) i))
        cb = (filter (>=x) j)
        db = (reverse (filter (<x) j))
