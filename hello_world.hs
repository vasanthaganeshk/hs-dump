import System.Environment

main = getArgs >>= (\args -> getLine >>= (\misc -> putStrLn ("Hello, " ++ (show args) ++ misc)))
