import Data.Array

main = print  (show  (g "XOXOXOXOXOXO"))
div12 = filter (12 `mod`) [1..12]

g a = map (f . uncurry) j
  where arr = listArray (1, 12) a
        j = map (\x -> (x, [1..x])) div12
        h = foldr (\a b-> b && ((arr ! a) == 'X'))
        f x [] = True
        f x (y:ys) = (h (filter (<=12) (map (x+) [0..11]))) && (f x ys)
